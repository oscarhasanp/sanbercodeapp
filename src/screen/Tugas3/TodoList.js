import React, { useState } from 'react';
import {
    Image,
    StyleSheet,
    View,
    Text,
    TextInput,
    Dimensions,
    TouchableOpacity,
    FlatList
} from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";

const TodoList = () => {
    const [list, setList] = useState([])
    const [text, setText] = useState("");
    return (
        <View style={styles.container}>
            <Text style={{marginHorizontal:10}}>Masukan TodoList</Text>
            <View style={styles.containerBox}>
                <View style={styles.boxInputGroup}>
                    <TextInput placeholder="input here" style={{ borderColor: 'black', borderWidth: 1, flex: 0.8 }}
                        onChangeText={(text) => { setText(text) }}
                    />
                    <TouchableOpacity style={{ marginLeft: 10, flex: 0.2, backgroundColor: '#42cef5', alignItems: 'center', justifyContent: 'center' }}
                        onPress={() => {
                            if (text) {
                                const date = new Date();
                                const fullDate = `${date.getDate()}/${date.getMonth()}/${date.getFullYear()}`
                                const title = text
                                setList([...list, { fullDate, title }])
                            }
                        }}
                    >
                        <Icon name="add" style={{ color: 'white' }} size={30}></Icon>
                    </TouchableOpacity>
                </View>
            </View>
            <FlatList
                style={{ marginHorizontal: 10 }}
                data={list}
                keyExtractor={(item, index) => index.toString()}
            
                renderItem={(val) => {
                    return (
                        <View style={styles.boxShadow}>
                            <View style={styles.cardBox}>
                                <View style={{ flexDirection: 'column', marginVertical: 15, marginHorizontal: 20 }}>
                                    <Text>{val.item.fullDate}</Text>
                                    <Text style={{ marginTop: 5 }}>{val.item.title}</Text>

                                </View>
                                <TouchableOpacity
                                    onPress={() => {
                                        
                                        list.splice(val.index, 1);
                                        setList([...list])
                                    }}
                                    style={{ alignSelf: 'center', marginHorizontal: 10 }}
                                >
                                    <Icon name="trash-outline" size={35} ></Icon>
                                </TouchableOpacity>
                            </View>
                        </View>
                    )
                }}
            >

            </FlatList>

        </View>
    )
}
const styles = StyleSheet.create({
    boxShadow: {
        borderColor:'black',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        
        elevation: 4,
    },
    cardBox: {

        width: '100%',
        flexDirection: "row",
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: 10,
    },
    boxInputGroup: {
        flexDirection: 'row',
    },
    container: {
        marginTop:20,
        flex: 1,
    },
    containerBox: {
        alignItems: 'center',
        marginVertical: 10,
        marginHorizontal: 10,
    }
})

export default TodoList;