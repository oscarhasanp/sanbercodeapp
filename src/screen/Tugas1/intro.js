import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

const app=()=>(
    <View style={styles.container}>
        <Text>Hello Kelas React Native Lanjutan Sanbercode!</Text>
    </View>
)
const styles=StyleSheet.create({
    container:{
        flex:1,
        justifyContent:"center",
        alignItems:'center'
    }
})
export default app;