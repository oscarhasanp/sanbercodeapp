import React from "react"
import {View,Text,StyleSheet,Image} from "react-native"

const SplashScreen=()=>{
    return(
        <View style={style.container}>
            <Image source={require("../../assets/images/logo.jpg")}/>
        </View>
    )
}
const style=StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    }
})
export default SplashScreen;
