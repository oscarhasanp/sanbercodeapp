import React from "react"

import "react-native-gesture-handler";
import { View, Text, Image, StatusBar, StyleSheet,ScrollView,TouchableOpacity } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import Icon from "react-native-vector-icons/Ionicons"
import Micon from "react-native-vector-icons/MaterialCommunityIcons"
import Chart from "./Chart"

const MenuNavigatorStack=createStackNavigator();

const styles = StyleSheet.create({
    classContainerInnerBody: {
        alignItems: 'center'
    },
    classContainerBody: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        backgroundColor: '#3EC6FF',
        borderBottomLeftRadius: 15,
        borderBottomRightRadius: 15,
        paddingVertical: 20
    },
    textHeader: {
        color: 'white',
        fontSize: 15
    },
    classContainerHeader: {
        backgroundColor: '#088dc4',
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
        padding: 10,
        paddingLeft: 25,
    },
    classContainer: {
        marginVertical: 10
    },
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: 'transparent',
        marginVertical: '10%',
    },
    styleContainer: {
        width: '96%',
        // backgroundColor:'blue'
    },
    summaryContainerHeader: {
        padding: 10,
        paddingLeft: 25,
        backgroundColor: '#3EC6FF',
    },
    summaryRow: {
        flexDirection: 'row',
        paddingVertical:10,
        paddingHorizontal:40,
        justifyContent: 'space-between',
        backgroundColor: '#088dc4'
    }

})
const Home = ({navigation}) => {
    const RenderSummary = () => (
        <View style={styles.classContainer}>
            <View style={styles.classContainerHeader}>
                <Text style={styles.textHeader}>Summary</Text>
            </View>
            <View style={styles.summaryContainerHeader}>
                <Text style={styles.textHeader}>React Native</Text>
            </View>
            <View style={styles.summaryRow}>
                <Text style={{color:'white'}}>Today</Text>
                <Text style={{color:'white'}}>20 Orang</Text>
            </View>
            <View style={styles.summaryRow}>
                <Text style={{color:'white'}}>Total</Text>
                <Text style={{color:'white'}}>100 Orang</Text>
            </View>
            <View style={styles.summaryContainerHeader}>
                <Text style={styles.textHeader}>Data Science</Text>
            </View>
            <View style={styles.summaryRow}>
                <Text style={{color:'white'}}>Today</Text>
                <Text style={{color:'white'}}>30 Orang</Text>
            </View>
            <View style={styles.summaryRow}>
                <Text style={{color:'white'}}>Total</Text>
                <Text style={{color:'white'}}>100 Orang</Text>
            </View>
            <View style={styles.summaryContainerHeader}>
                <Text style={styles.textHeader}>React JS</Text>
            </View>
            <View style={styles.summaryRow}>
                <Text style={{color:'white'}}>Today</Text>
                <Text style={{color:'white'}}>66 Orang</Text>
            </View>
            <View style={styles.summaryRow}>
                <Text style={{color:'white'}}>Total</Text>
                <Text style={{color:'white'}}>100 Orang</Text>
            </View>
            <View style={styles.summaryContainerHeader}>
                <Text style={styles.textHeader}>Laravel</Text>
            </View>
            <View style={styles.summaryRow}>
                <Text style={{color:'white'}}>Today</Text>
                <Text style={{color:'white'}}>60 Orang</Text>
            </View>
            <View style={styles.summaryRow}>
                <Text style={{color:'white'}}>Total</Text>
                <Text style={{color:'white'}}>100 Orang</Text>
            </View>
        </View>
    )
    return (
        <ScrollView>
        <View style={styles.container}>
            <View style={styles.styleContainer}>

                <View style={styles.classContainer}>
                    <View style={styles.classContainerHeader}>
                        <Text style={styles.textHeader}>Kelas</Text>
                    </View>
                    <View style={styles.classContainerBody}>
                        <TouchableOpacity style={styles.classContainerInnerBody} onPress={()=>{navigation.push("React Native")}}>
                            <Icon name='logo-react' size={35} style={{ color: 'white' }}></Icon>
                            <Text style={{ color: 'white', fontSize: 15 }}>React Native</Text>
                        </TouchableOpacity>
                        <View style={styles.classContainerInnerBody}>
                            <Icon name='logo-python' size={35} style={{ color: 'white' }}></Icon>
                            <Text style={{ color: 'white', fontSize: 15 }}>Data Science</Text>
                        </View>
                        <View style={styles.classContainerInnerBody}>
                            <Icon name='logo-react' size={35} style={{ color: 'white' }}></Icon>
                            <Text style={{ color: 'white', fontSize: 15 }}>React JS</Text>
                        </View>
                        <View style={styles.classContainerInnerBody}>
                            <Icon name='logo-laravel' size={35} style={{ color: 'white' }}></Icon>
                            <Text style={{ color: 'white', fontSize: 15 }}>Laravel</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.classContainer}>
                    <View style={styles.classContainerHeader}>
                        <Text style={styles.textHeader}>Kelas</Text>
                    </View>
                    <View style={styles.classContainerBody}>
                        <View style={styles.classContainerInnerBody}>
                            <Icon name='logo-wordpress' size={35} style={{ color: 'white' }}></Icon>
                            <Text style={{ color: 'white', fontSize: 15 }}>Wordpress</Text>
                        </View>
                        <View style={styles.classContainerInnerBody}>
                            <Image source={require('../../assets/images/ux.png')} style={{ width: 38, height: 38 }}></Image>
                            <Text style={{ color: 'white', fontSize: 15 }}>Design Grafis</Text>
                        </View>
                        <View style={styles.classContainerInnerBody}>
                            <Micon name='server' size={38} style={{ color: 'white' }}></Micon>
                            <Text style={{ color: 'white', fontSize: 15 }}>Web Server</Text>
                        </View>
                        <View style={styles.classContainerInnerBody}>
                            <Image source={require('../../assets/images/website-design.png')} style={{ width: 38, height: 38 }}></Image>

                            <Text style={{ color: 'white', fontSize: 15 }}>UI/UX Design</Text>
                        </View>
                    </View>
                </View>
                <RenderSummary></RenderSummary>
            </View>
        </View>
        </ScrollView>
    )
}

const MenuNavigatorScreen=()=>{
    return(
        <MenuNavigatorStack.Navigator>
            <MenuNavigatorStack.Screen name="Home" component={Home}></MenuNavigatorStack.Screen>
            <MenuNavigatorStack.Screen name="React Native" options={{headerShown:true}} component={Chart}></MenuNavigatorStack.Screen>
        </MenuNavigatorStack.Navigator>
    )
}
export default MenuNavigatorScreen