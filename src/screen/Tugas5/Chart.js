import React from "react"
import {View,processColor} from "react-native"
import {BarChart} from "react-native-charts-wrapper"

const yMarker=(data)=>{
    return{
        y:data,
        marker:[`React Native Dasar \n${data[0]}`,`React Native Lanjutan \n${data[1]}`]
    }
}
const Chart=()=>{
    const data = [
        yMarker([100, 40]),
        yMarker([80, 60]),
        yMarker([40, 90]),
        yMarker([78, 45]),
        yMarker([67, 87]),
        yMarker([98, 32]),
        yMarker([150, 90]),
    ]
    
    const[chart,legend,xAxis,yAxis]=[{
        data: {
            dataSets: [{
                values: data,
                label: '',
                config: {
                    colors: [processColor('#088dc4'), processColor('#3EC6FF')],
                    stackLabels: ['React Native Dasar', 'React Native Lanjutan'],
                    drawFilled: false,
                    drawValues: false,
                }
            }]
        }
    },{
        enabled: true,
        textSize: 14,
        form: 'SQUARE',
        formSize: 14,
        xEntrySpace: 10,
        yEntrySpace: 5,
        formToTextSpace: 5,
        wordWrapEnabled: true,
        maxSizePercent: 0.5
 },
 {
    valueFormatter: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Agu', 'Sep', 'Oct', 'Nov', 'Des'],
    position: 'BOTTOM',
    drawAxisLine: true,
    drawGridLines: false,
    axisMinimum: -0.5,
    granularityEnabled: true,
    granularity: 1,
    axisMaximum: new Date().getMonth() + 0.5,
    spaceBetweenLabels: 0,
    labelRotationAngle: -45.0,
    limitLines: [{ limit: 115, lineColor: processColor('red'), lineWidth: 1 }]
},
{
    left: {
        axisMinimum: 0,
        labelCountForce: true,
        granularity: 5,
        granularityEnabled: true,
        drawGridLines: false
    },
    right: {
        axisMinimum: 0,
        labelCountForce: true,
        granularity: 5,
        granularityEnabled: true,
        enabled: false
    }
}
]
    return(
        <View style={{flex:1}}>
            <BarChart
                style={{flex:1}}
                data={chart.data}
                legend={legend}
                xAxis={xAxis}
                yAxis={yAxis}
                marker={{enabled:true}}
            >

            </BarChart>
        </View>
    )
}
export default Chart