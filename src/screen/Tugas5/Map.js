import React, { useEffect } from "react"
import {View} from "react-native"
import MapboxGL from "@react-native-mapbox-gl/maps"

MapboxGL.setAccessToken("pk.eyJ1Ijoib3NjYXJwIiwiYSI6ImNrZGcxczQ3dTBtOTUydHBmdDVvM2E0eWoifQ.aygxdD6f1NNXSpK6DFVTMQ")
const coordinates = [
    [107.598827, -6.896191],
    [107.596198, -6.899688],
    [107.618767, -6.902226],
    [107.621095, -6.898690],
    [107.615698, -6.896741],
    [107.613544, -6.897713],
    [107.613697, -6.893795],
    [107.610714, -6.891356],
    [107.605468, -6.893124],
    [107.609180, -6.898013]
]
const Maps=()=>{
    useEffect(()=>{
        const getLocation=async()=>{
            try {
                const permit=await MapboxGL.requestAndroidLocationPermissions()
            } catch (error) {
                console.log(error)
            }
        }
        getLocation()
    })
    return(<View style={{flex:1}}>
        <MapboxGL.MapView style={{flex:1}}>
            <MapboxGL.UserLocation visible={true}></MapboxGL.UserLocation>
            <MapboxGL.Camera followUserLocation={true}></MapboxGL.Camera>
            
            {coordinates.map((val,index)=>{
                return(<MapboxGL.PointAnnotation
                key={index.toString()}
                    id={`point${index}`}
                    coordinate={val}
                >
                    <MapboxGL.Callout title={`Longitude : ${val[0]} \n Latitude : ${val[1]}`}></MapboxGL.Callout>
                </MapboxGL.PointAnnotation>)
            })}
        </MapboxGL.MapView>
    </View>)    
}
export default Maps 