import React, { useEffect,useState } from 'react';

import 'react-native-gesture-handler';
import {
    Image,
    StyleSheet,
    View,
    Text,
    Dimensions,TextInput,
    Modal,
    TouchableOpacity
} from 'react-native';
import {RNCamera} from "react-native-camera"
import Icon from "react-native-vector-icons/Feather"
import storage from "@react-native-firebase/storage"
import IconComunity from 'react-native-vector-icons/MaterialCommunityIcons'
const height = Dimensions.get("window").height;
const width = Dimensions.get("window").width;
const Register = ({navigation}) => {
    const [isVisible,setVisible]=useState(false)
    const [type,setType]=useState('back')
    const [photo,setPhoto]=useState(null)
    const setToogleCamera=()=>{
        setType(type ==='back'?'front':'back')
    }
    var camera=null;
    const takePicture=async()=>{
        if(camera){
            const data=await camera.takePictureAsync({quality:0.5,base64:true})
            setPhoto(data)
            setVisible(false)
        }
    }
    const uploadFoto=(uri)=>{
        const time=new Date().getTime()
        storage().ref(`photos/${time}`)
        .putFile(uri)
        .then(res=>{
            alert('upload success')
        }).catch(err=>{
            console.log(err.message)
            alert('Upload failed')
        })
    }
    const RenderCamera=()=>{
        return(
            <Modal visible={isVisible} onRequestClose={()=>{setVisible(false)}}>
                <View style={{flex:1}}>
                    <RNCamera style={{flex:1}}
                        ref={ref=>{
                            camera=ref;      
                        }}
                        type={type}
                    >
                        <View style={{margin:20}}>
                            <TouchableOpacity onPress={()=>{
                                setToogleCamera()
                            }}
                            style={{justifyContent:'center',alignItems:'center',backgroundColor:'white',height:50,width:50,borderRadius:50}}>
                                <IconComunity name='rotate-3d-variant' size={25}></IconComunity>
                            </TouchableOpacity>
                        </View>
                        <View style={{alignItems:'center'}}>
                            <View style={{width:'50%',height:'45%',borderRadius:100,borderWidth:1,borderColor:'white'}}></View>
                            <View style={{marginTop:60,width:'50%',height:'20%',borderWidth:1,borderColor:'white'}}></View>
                            <TouchableOpacity
                            onPress={()=>{
                                takePicture()
                            }}
                             style={{marginTop:30,height:80,width:80,borderRadius:50,backgroundColor:'white',justifyContent:'center',alignItems:'center' }}>
                                <Icon name='camera' size={35}></Icon>
                            </TouchableOpacity>
                        </View>
                    </RNCamera>
                </View>
            </Modal>
        )
    }
    return(
    <View style={styles.container}>
        <View style={styles.containerPhoto}>
            <Image source={photo?{uri:photo.uri}:require('../../assets/images/profil.jpg')} style={{height:100,width:100,borderRadius:50}} />
            <TouchableOpacity onPress={()=>{
                setVisible(true)
            }}>
                <Text style={{color:'white',fontSize:23,marginVertical:20}}>Change Picture</Text>
            </TouchableOpacity>
        </View>
        <View>
            <View style={styles.boxFloat}>
            <View style={styles.inputContainer}>
                <Text>Nama</Text>
                <TextInput style={styles.textInput} placeholder="Name"/>
            </View>
            <View style={styles.inputContainer}>
                <Text>Email</Text>
                <TextInput style={styles.textInput} placeholder="Email"/>
            </View>
            <View style={styles.inputContainer}>
                <Text>Password</Text>
                <TextInput secureTextEntry={true} style={styles.textInput} placeholder="Password"/>
            </View>
                <TouchableOpacity
                onPress={()=>{
                    if(photo)
                        uploadFoto(photo.uri)
                    else alert("Tidak Ada Photo untuk diupload")
                }}
                 style={{backgroundColor:'#3EC6FF',marginHorizontal:40,padding:5,marginVertical:15}} >
                    <Text style={{textAlign:'center',color:'white',padding:5}}>REGISTER</Text>
                </TouchableOpacity>
            </View>
        </View>
        <RenderCamera></RenderCamera>
    </View>
)}
const styles = StyleSheet.create({
    inputContainer:{
        marginVertical:10,
        marginHorizontal:35
    },
    btnFlipContainer:{

    },
    textInput:{
        borderBottomWidth:1,
        padding:-5
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    containerPhoto: {
        width: '100%',
        height: '40%',
        backgroundColor: '#3EC6FF',
        alignItems:'center',
        justifyContent:'center'
    },
    boxFloat: {
        position: 'absolute',
        top: (0 - height * 0.05),
        left: (0 - (width * 0.45)),
        width: "90%",
        height: height * 0.4,
        backgroundColor:'white',
        borderRadius:10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.39,
        shadowRadius: 8.30,

        elevation: 13,
    }
})
export default Register;