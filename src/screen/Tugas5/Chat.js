import React, { useState , useEffect} from "react"
import {View} from "react-native"
import {GiftedChat} from "react-native-gifted-chat"
import database from "@react-native-firebase/database"
import auth from "@react-native-firebase/auth"

const Chat=()=>{
    const [pesan,setPesan]= useState([])
    const [user,setUser]=useState({})
    useEffect(()=>{
        getUser()
        database().ref('messages').limitToLast(20).on('child_added',(snapshot)=>{
            
            setPesan(pesanList=>{ return GiftedChat.append(pesanList,snapshot.val())})
            
        })
        return ()=>{
            const db=database().ref('messages')
            if(db) db.off()
        }
    },[])
    const getUser=()=>{
        const userTemp=auth().currentUser
        setUser(userTemp)
    }
    const SendingMsg=(pesan=[])=>{
        pesan.map(val=>{
            database().ref('messages').push({
                _id:val._id,
                createdAt:database.ServerValue.TIMESTAMP,
                text:val.text,
                user:val.user
            })
        })
    }
    return (
        <GiftedChat
            messages={pesan}
            onSend={(msg)=>{SendingMsg(msg)}}
            user={{
                _id:user.uid,
                name:user.email,
                avatar:'https://store.playstation.com/store/api/chihiro/00_09_000/container/US/en/99/UP1675-CUSA11816_00-AV00000000000012//image?_version=00_09_000&platform=chihiro&w=720&h=720&bg_color=000000&opacity=100'
            }}
        >

        </GiftedChat>
    )
}
export default Chat