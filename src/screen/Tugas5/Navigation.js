
import React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from "react-native-vector-icons/MaterialIcons";
import Asyncstorage from "@react-native-community/async-storage"
import EntypoIcon from "react-native-vector-icons/Entypo";
import firebase from "@react-native-firebase/app"
import SplashScreen from './SplashScreen';
import Intro from './Intro';
import Login from './Login';
import Profil from "../Tugas2/Biodata"
import styles from './style';
import Register from "./Register"
import Map from "./Map"
import Chat from "./Chat"
import BodyScreen from "./Home"
const BottomStackNav = createBottomTabNavigator();

const Stack = createStackNavigator();
const LoginStack = createStackNavigator();
// const BodyScreen = () => (
   
// )
const MenuBottom = () => (
    <BottomStackNav.Navigator initialRouteName="Profile">
        <BottomStackNav.Screen name="Menu" options={{
            tabBarLabel: 'Home',
            tabBarIcon: ({ tintColor }) => (
                <Icon name="home" size={25} />
            )
        }} component={BodyScreen} />
        <BottomStackNav.Screen name="Map" options={{
            tabBarLabel: 'Map',
            tabBarIcon: ({ tintColor }) => (
                <Icon name="pin-drop" size={25} />
            )
        }} component={Map} />
        <BottomStackNav.Screen name="Chat" options={{
            tabBarLabel: 'Chat',
            tabBarIcon: ({ tintColor }) => (
                <EntypoIcon name="chat" size={25} />
            )
        }} component={Chat} />
        <BottomStackNav.Screen name="Profile" options={
            {
                tabBarLabel: 'Profil',
                tabBarIcon: () => (
                    <Icon name="supervisor-account" size={25} />
                ),
            }
        } component={Profil} />
    </BottomStackNav.Navigator>
)
const LoginStackMenu = () => (
    <LoginStack.Navigator headerMode={false}>
        <LoginStack.Screen name="Login" component={Login}></LoginStack.Screen>
        <LoginStack.Screen name="Profile" component={MenuBottom}></LoginStack.Screen>
    </LoginStack.Navigator>
)
const MainNavigation = () => (
    <Stack.Navigator headerMode={false}>
        <Stack.Screen name="Intro" component={Intro} options={{ headerShown: false }} />
        <Stack.Screen name="Register" component={Register}></Stack.Screen>
        <Stack.Screen name="Login" component={LoginStackMenu} options={{ headerShown: false }} />
    </Stack.Navigator>
)
var firebaseConfig = {
    apiKey: "AIzaSyCSgpPQCdiHMRq_oJL1N3T4vTcCrDGWG_E",
    authDomain: "sanbercodeapp-b7a66.firebaseapp.com",
    databaseURL: "https://sanbercodeapp-b7a66.firebaseio.com",
    projectId: "sanbercodeapp-b7a66",
    storageBucket: "sanbercodeapp-b7a66.appspot.com",
    messagingSenderId: "704951634305",
    appId: "1:704951634305:web:6dfadc86590e9f8c864080",
    measurementId: "G-06BCZGGEPK"
  };
  if(!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig)
  }
  
function Navigation() {
    const [isLoading, setIsLoading] = React.useState(true)
    const [view,setView]=React.useState(null)
    //mengatur durasi splashscreen saat aplikasi pertama kali dibuka
    React.useEffect(() => {
        async function fetchData(){
            
            const statusApp=await Asyncstorage.getItem("statusApp");
            // console.log("fetching")
            // console.log("isLoading -> ",isLoading)
            return new Promise((resolve,reject)=>{
                
                console.log('status app -> ',statusApp)
                if(statusApp=='1')
                    setIsLoading(prev=>{
                        resolve(false)
                        return false
                    })
                else if(isLoading){
                    setTimeout(() => {
                        console.log("ap")
                        setIsLoading(false)
                    }, 3000)
                }
                
                resolve(isLoading)
            })
        }
        fetchData().then(status=>{
            if (status) {
                return setView(<SplashScreen />)
            }else{
                return setView(<NavigationContainer>
                    <MainNavigation />
                </NavigationContainer>)
            }
            
        })
        // console.log("hello")
    }, [isLoading])
    
    return view
    // fetchData().then
   
}

export default Navigation;
