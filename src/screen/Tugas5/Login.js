import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    Button,
    TextInput,
    StyleSheet,
    TouchableOpacity
} from 'react-native';
import'react-native-gesture-handler';
import Asyncstorage from "@react-native-community/async-storage";
import auth from "@react-native-firebase/auth"
import TouchID from "react-native-touch-id";
import {GoogleSignin,statusCodes,GoogleSigninButton} from "@react-native-community/google-signin"
import axios from "axios"
axios.defaults.baseURL="https://mainbersama.demosanbercode.com/api"
// export const api="https://mainbersama.demosanbercode.com/api"
const styles=StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'white'
    },
    inputStyle:{
        width:'100%',
        borderBottomWidth:1,
        marginVertical:5,
    },
    buttonStyle:{
        width:'100%',
        backgroundColor:'blue',
        padding:10,
        marginVertical:10
    },
    textBottom:{
        marginVertical:20,
        alignItems:'center'
    },
    
})
const Login=({navigation})=>{
    const [email,setEmail]=useState("")
    const [password,setPassword]=useState("")
    const [status,setStatus]=useState(true)

    useEffect(()=>{
        googleSignInConfig()
        Asyncstorage.setItem("statusApp","1")
        async function fetch(){
            const isLogin=await Asyncstorage.getItem('isLogin')
            if(isLogin=='1'){
                loginClick()
            }
        }
        fetch()
    },[])
    const fingerPrintLogin=()=>{
        TouchID.authenticate("",{
            title:'Authentication Required',
            imageColor:'#191970',
            imageErrorColor:'red',
            sensorDescription:'Touch Sensor',
            sensorErrorDescription:'Failed',
            cancelText:'Cancel'
        }).then(success=>{
            navigation.navigate('Profile')
        }).catch(err=>{
            alert('Authentication Failed')
        })
    }
    const googleSignInConfig=()=>{
        GoogleSignin.configure({
            offlineAccess:false,
            webClientId:'704951634305-4js72t2d196e034q5ghoqofej623j5gl.apps.googleusercontent.com'
        })
    }
    const signInGoogle=async()=>{
        try {
            const {idToken}=await GoogleSignin.signIn();
            const credential=auth.GoogleAuthProvider.credential(idToken)
            auth().signInWithCredential(credential);
            navigation.navigate('Profile')
        } catch (error) {
            
        }
    }
    const loginClick=()=>{
        Asyncstorage.setItem('isLogin','1')
        navigation.reset({
            index:0,
            routes:[{name:'Profile'}]
        })
        // auth().signInWithEmailAndPassword(email,password).then(res=>{
        //     navigation.push('Profile')
        // }).catch(err=>{
        //     console.log(err)
        // })
    }
    // const loginClick= ()=>{
    //     axios.post("login",{email,password},{timeout:20000}).then((res)=>{
    //         async function saveToken(token){
    //            await Asyncstorage.setItem("token",token)
    //         }
    //         saveToken(res.data.token)
    //         navigation.navigate("Profile")
    //     }).catch((err)=>{
    //         console.log(err)
    //         setStatus(false)
    //     })
    // }
    return(
        <View style={styles.container}>
            <Image style={{alignSelf:'center'}} source={require("../../assets/images/logo.jpg")}/>
            <View style={{margin:15}}>
                <Text>Username</Text>
                <TextInput style={styles.inputStyle} placeholder="Username or Email"
                onChangeText={(text)=>{
                    setEmail(text)
                }}
                value={email}
                ></TextInput>
            </View>
            <View style={{marginHorizontal:15}}>
                <Text>Password</Text>
                <TextInput style={styles.inputStyle} placeholder="Password" secureTextEntry={true}
                onChangeText={(text)=>{
                    setPassword(text)
                }}
                value={password}
                ></TextInput>
            </View>
            <View style={{margin:20}}>
            <TouchableOpacity style={styles.buttonStyle} onPress={loginClick}>
                    <Text style={{color:'white',textAlign:'center'}}>LOGIN</Text>
            </TouchableOpacity>
            
            
                
            {!status?<View style={{flexDirection:'row',alignItems:'center'}}><View style={{flex:0.5,height:1,backgroundColor:'black'}}></View>
                
                <Text style={{marginHorizontal:5,color:'red'}}>Gagal Login</Text>
                
                <View style={{flex:0.5,height:1,backgroundColor:'black'}}></View>
                

            </View>
            :<View></View>
            }
            <GoogleSigninButton
                style={{width:'100%',height:40}}
                size={GoogleSigninButton.Size.Wide}
                color={GoogleSigninButton.Color.Dark}
                onPress={()=>{signInGoogle()}}
            >

            </GoogleSigninButton>
            <TouchableOpacity style={{...styles.buttonStyle,backgroundColor:'#191970'}} onPress={()=>fingerPrintLogin()}>
                    <Text style={{color:'white',textAlign:'center'}}>SIGN IN WITH FINGERPRINT</Text>
            </TouchableOpacity>
            <View style={styles.textBottom}>
                <Text> Belum Mempunyai Akun? <TouchableOpacity
                onPress={()=>{
                    navigation.navigate('Register')
                }}
                ><Text style={{color:'blue'}}>Buat Akun</Text>
                </TouchableOpacity></Text>
            </View>
            </View>
            
        </View>
    )
}
export default Login;