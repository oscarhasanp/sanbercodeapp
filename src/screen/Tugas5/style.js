import {StyleSheet} from "react-native"
const styles=StyleSheet.create({
    container:{
        flex:1,
    },
    slide:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    title:{
        fontSize:25,
        color:'purple',
        fontWeight:'bold'
    },
    text:{
        padding:20,
        textAlign:'center',
        color:'black'
    },
    buttonCircle:{
        width:50,
        height:50,
        backgroundColor:'purple',
        borderRadius:100,
        justifyContent:'center',
        alignItems:'center'
    }
})
export default styles