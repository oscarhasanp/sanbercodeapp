import React, { useEffect, useState } from 'react';
import 'react-native-gesture-handler';
import {
    Image,
    StyleSheet,
    View,
    Text,
    Dimensions,
    TouchableOpacity,
} from 'react-native';
import Asyncstorage from "@react-native-community/async-storage"
import auth from "@react-native-firebase/auth"
import { GoogleSignin, } from "@react-native-community/google-signin"
const height = Dimensions.get("window").height;
const width = Dimensions.get("window").width;
const Biodata = ({ navigation }) => {
    const [user, setUser] = useState({})
    useEffect(() => {
        getUser()
    }, [])
    const getUser = async () => {
        try {
            const userLogged = await GoogleSignin.signInSilently()
            console.log("user Logged in -> ", userLogged.user)
            setUser(userLogged.user)
        } catch (error) {
            console.log(error)
        }
    }
    const onLogout = async () => {
        try {
            await GoogleSignin.revokeAccess()
            await GoogleSignin.signOut()
        } catch (error) {
            console.log(error)
        }
        await Asyncstorage.removeItem("token")
        await Asyncstorage.removeItem('isLogin')
        navigation.navigate('Login')
    }
    return (
        <View style={styles.container}>
            <View style={styles.containerPhoto}>
                <Image source={{ uri: user && user.photo }} style={{ height: 100, width: 100, borderRadius: 50 }} />
                <Text style={{ color: 'white', fontSize: 23, marginVertical: 20 }}>{user && user.name}</Text>
            </View>
            <View>
                <View style={styles.boxFloat}>
                    <View style={styles.textContainer}>
                        <Text>Tanggal Lahir</Text>
                        <Text>11 Oktober 1999</Text>
                    </View>
                    <View style={styles.textContainer}>
                        <Text>Jenis Kelamin</Text>
                        <Text>Laki-Laki</Text>
                    </View>
                    <View style={styles.textContainer}>
                        <Text>Hobi</Text>
                        <Text>Berenang</Text>
                    </View>
                    <View style={styles.textContainer}>
                        <Text>No. Telp</Text>
                        <Text>085716861242</Text>
                    </View>
                    <View style={styles.textContainer}>
                        <Text>Email</Text>
                        <Text>{user && user.email}</Text>
                    </View>
                    <TouchableOpacity style={{ backgroundColor: 'blue', marginHorizontal: 40, padding: 5, marginVertical: 15 }} onPress={onLogout}>
                        <Text style={{ textAlign: 'center', color: 'white' }}>LogOut</Text>
                    </TouchableOpacity>
                </View>
            </View>

        </View>
    )
}
const styles = StyleSheet.create({
    textContainer: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'space-between',
        marginHorizontal: 20,
        marginVertical: 10,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    containerPhoto: {
        width: '100%',
        height: '40%',
        backgroundColor: '#3EC6FF',
        alignItems: 'center',
        justifyContent: 'center'
    },
    boxFloat: {
        position: 'absolute',
        top: (0 - height * 0.05),
        left: (0 - (width * 0.45)),
        width: "90%",
        height: height * 0.4,
        backgroundColor: 'white',
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.39,
        shadowRadius: 8.30,

        elevation: 13,
    }
})
export default Biodata;