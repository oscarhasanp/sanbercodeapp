import React, { createContext, Provider,useState} from 'react';
import {
    View,
    Text,
    
} from 'react-native';
import TodoList from "./TodoList"
export const Root=createContext(); 

const App=React.memo(()=>{
    const [list, setList] = useState([])
    const [text, setText] = useState("");
    
    const addTodo=()=>{
        if (text) {
            const date = new Date();
            const fullDate = `${date.getDate()}/${date.getMonth()}/${date.getFullYear()}`
            const title = text
            setList([...list, { fullDate, title }])
        }
    }
    const handleChange=(text)=>{
        setText(text)
    }
    const deleteTodo=(index)=>{
        
        list.splice(index, 1);
        setList([...list])
    }
    const valueContext={
        list,
        text,
        addTodo,
        handleChange,
        deleteTodo
    }
    return(
        <Root.Provider value={valueContext}>
            <TodoList/>
        </Root.Provider>
    )
}
)
export default App;